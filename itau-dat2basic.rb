#!/usr/bin/env ruby

require 'date'

def transaction_data?(description)
  ![/^CONCEPTO/, /^SALDO INICIAL/, /^SALDO FINAL/].
    any? { |e| description.to_s.strip.match?(e) }
end

# We'll read from STDIN and write to STDOUT.
STDIN.each_line do |line|
  data = line.chomp.unpack("a7a4a7a2a15a15a*")
  row = {
    "Date"   => Date.parse(data[2]),
    "Amount" => data[5].to_f - data[4].to_f,
    "Memo"   => data[6].gsub(/\s\s*/, " ")
  }
  puts row.values_at(*%w(Date Amount Memo)).join(",") if transaction_data?(row["Memo"])
end
