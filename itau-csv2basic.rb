#!/usr/bin/env ruby

require 'csv'
require 'date'

CSV::Converters[:itau_time] = lambda {|s| 
  return s unless /^\d\d\D\D\D\d\d$/ =~ s

  begin 
    Date.parse(s)
  rescue ArgumentError
    s
  end
}

CSV::Converters[:simpler_strings] = lambda {|s| 
  begin 
    s.gsub(/\s\s*/, " ")
  rescue ArgumentError
    s
  end
}

def is_a_data_row?(row)
  concepto = row["Concepto"].to_s.strip
  concepto != "CONCEPTO" && concepto != "SALDO INICIAL" && concepto != "SALDO FINAL"
end

headers = %w(Cuenta Moneda Fecha XX Debe Haber Concepto)

# We'll read from STDIN and write to STDOUT.
CSV($stdin, row_sep: "\r\n", headers: headers, converters: [:all, :simpler_strings, :itau_time]) do |csv_in|
  csv_in.each do |row|
    if is_a_data_row?(row)
      row << ["Monto", row["Haber"] - row["Debe"]]
      puts row.fields("Fecha", "Monto", "Concepto").to_csv
    end
  end
end
