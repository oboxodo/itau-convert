#!/usr/bin/env ruby

require 'csv'

FLOAT_NUMBER_REGEX = /^[+-]?([0-9]{1,3}(?:(?<thousand>[,\.]?)[0-9]{3})?(?:\k<thousand>[0-9]{3})*)?(?:(?<decimal>[,\.])[0-9]+)?$/

CSV::Converters[:float_numbers] = lambda {|s| 
  return s unless !s.strip.empty? and m = FLOAT_NUMBER_REGEX.match(s)

  begin 
    s.tr(m[:thousand].to_s, "").tr(m[:decimal].to_s, ".").to_f
  rescue ArgumentError
    s
  end
}

def should_be_processed?(row, opts = {})
  return false if opts[:only_currency] && opts[:only_currency] != row["Currency"]

  opts[:exclude_tags] ||= []

  tags = row["Entry (tags)"].split(",").map {|i| i.strip}
  (tags & opts[:exclude_tags]).empty?
end

# Toshl headers:
# "Date","Entry (tags)","Expense amount","Currency","Amount in main currency","Main currency","Description"

ynab_fields = %w(Date Payee Category Memo Outflow Inflow)

# Print headers
puts ynab_fields.to_csv

# We'll read from STDIN and write to STDOUT.
csv_in = []
CSV.foreach(ARGF.file, encoding: "bom|utf-8", headers: true, return_headers: false, converters: [:all, :float_numbers]) {|row| csv_in << row}
CSV::Table.new(csv_in.reverse).each do |row|
  if should_be_processed?(row, exclude_tags: ["debito", "credito"], only_currency: "UYU")
    memo = row["Entry (tags)"]
    memo += ": " + row["Description"] unless row["Description"].strip.empty?
    row << ["Date", row["Date"]]
    row << ["Payee", ""]
    row << ["Category", ""]
    row << ["Memo", memo.tr("\n"," ")] # YNAB doesn't import new lines.
    row << ["Outflow", row["Expense amount"]]
    row << ["Inflow", 0]
    puts row.fields(*ynab_fields).to_csv
  end
end
