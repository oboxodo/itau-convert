#!/usr/bin/env ruby

require 'csv'

headers_in  = %w(Date Amount Memo)
headers_out = %w(Date Payee Category Memo Outflow Inflow)

# Print headers
puts headers_out.to_csv

# We'll read from STDIN and write to STDOUT.
CSV($stdin, headers: headers_in, converters: [:numeric, :date]) do |csv_in|
  csv_in.each do |row|
    row << ["Payee", row["Memo"]]
    row << ["Category", ""]
    row << ["Outflow", [0, row["Amount"]].min * -1]
    row << ["Inflow",  [0, row["Amount"]].max]
    puts row.fields(*headers_out).to_csv
  end
end
